ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG APKVER

RUN apk update \
&& apk upgrade --available --no-cache \
&& apk add --no-cache --upgrade clamav-daemon clamav-libunrar

WORKDIR /etc/clamav
COPY etc ./

SHELL [ "/bin/ash", "-o", "pipefail", "-c" ]
RUN crontab -d -u root && echo "0 */6 * * * /usr/bin/freshclam" | crontab -u clamav -

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh container-scripts/health-nc.sh entrypoint.sh ./

CMD [ "entrypoint.sh" ]
VOLUME /var/lib/clamav
EXPOSE 3310

HEALTHCHECK --start-period=6m --interval=6m --timeout=6m \
  CMD health-nc.sh PING 3310 PONG || exit 1
