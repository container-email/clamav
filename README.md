# clamav
Alpine based Dockerfile for running [clamav](https://www.clamav.net) in a container image.

Uses alpine cron and freshclam to update the virus database four times a day.

[![Docker Pulls](https://img.shields.io/docker/pulls/a16bitsysop/clamav.svg?style=plastic)](https://hub.docker.com/r/a16bitsysop/clamav/)
[![Docker Stars](https://img.shields.io/docker/stars/a16bitsysop/clamav.svg?style=plastic)](https://hub.docker.com/r/a16bitsysop/clamav/)
[![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/a16bitsysop/clamav/latest?style=plastic)](https://hub.docker.com/r/a16bitsysop/clamav/)
[![Release Commit SHA](https://img.shields.io/badge/dynamic/json.svg?label=release%20commit%20SHA&style=plastic&color=orange&query=sha&url=https://gitlab.com/container-email/clamav/-/raw/main/badges.json)](https://gitlab.com/container-email/clamav/)

## GitLab
GitLab Repository: [https://gitlab.com/container-email/clamav](https://gitlab.com/container-email/clamav)

## Environment Variables
| Name     | Desription                                             | Default |
|----------|--------------------------------------------------------|---------|
| TIMEZONE | Timezone to use inside the container, eg Europe/London | unset   |

## Examples
**To run a container with tmpfs mount on /tmp**
```bash
docker container run --mount type=tmpfs,destination=/tmp -p 3310:3310 \
-d --name clamav a16bitsysop/clamav
```
